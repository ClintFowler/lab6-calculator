package Calculator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Clinton on 7/23/2015.
 */
public class Calculator extends JFrame
{
    private JTextField displayField;
    private double value1 = 0;
    private double value2 = 0;
    private boolean val1Set = false;
    private boolean canClearInputField = false;
    private boolean operandSelected = false;
    private char setOperand;
    private char[] numbers = new char[]{'0','1','2','3','4','5','6','7','8','9'};
    private char[] operands = new char[]{'.','C','/','*','-','+','='};

    public Calculator()
    {
        super();
        this.setSize(300, 300);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("My Calculator");
        this.setLayout(new BorderLayout());
        this.setAutoRequestFocus(true);
        this.setResizable(false);
        setOperand = 'N';
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    checkOperandState('=');
                }
                else
                {
                    checkInput(e.getKeyChar());
                }
            }
        });
        setupLayout();

        this.setVisible(true);
    }

    private void setupLayout()
    {
        displayField = new JTextField();
        displayField.setEditable(false);
        displayField.setFocusable(false);
        displayField.setHorizontalAlignment(JTextField.RIGHT);
        displayField.setSize(25, 25);
        displayField.setText("0");
        this.add(displayField, BorderLayout.NORTH);

        JPanel buttonpane = new JPanel();
        buttonpane.setSize(300, 275);
        buttonpane.setLayout(new BoxLayout(buttonpane,BoxLayout.LINE_AXIS));
        this.add(buttonpane, BorderLayout.CENTER);

        JPanel[] buttonpanels = new JPanel[4];

        for (int i = 0; i<4; i++)
        {
            buttonpanels[i] = new JPanel();
            buttonpanels[i].setSize(75,275);
            buttonpanels[i].setLayout(new GridLayout(0,1));
            buttonpane.add(buttonpanels[i]);
        }

        myNumberButtonListener listen = new myNumberButtonListener();
        JButton[] numbs = new JButton[10];
        JButton[] opers = new JButton[7];

        for(int i = 0; i<10; i++)
        {
            numbs[i] = new JButton("" + numbers[i]);
            numbs[i].addActionListener(listen);
            numbs[i].setFocusable(false);
            numbs[i].setSize(55,55);
        }

        for(int i = 0; i<7; i++)
        {
            opers[i] = new JButton("" + operands[i]);
            opers[i].addActionListener(listen);
            opers[i].setFocusable(false);
            opers[i].setSize(55,55);
        }

        for (int i = 0; i<3; i++)
        {
            for (int j = 7 + i; j > 0 + i; j -= 3)
            {
                buttonpanels[i].add(numbs[j]);
            }
        }
        buttonpanels[0].add(numbs[0]);
        buttonpanels[1].add(opers[0]);
        buttonpanels[2].add(opers[1]);
        for(int i = 2; i<7; i++)
        {
            buttonpanels[3].add(opers[i]);
        }

    }

    private class myNumberButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            JButton button = (JButton) e.getSource();
            checkInput((button.getText().toCharArray())[0]);
        }
    }

    private void addNumber(char num)
    {
        clearInputField();
        if(displayField.getText().equals("0"))
        {
            if(num != '0')
            {
                displayField.setText("" + num);
            }
        }
        else
        {
            displayField.setText(displayField.getText() + num);
        }
    }

    private void convertToDouble()
    {
        System.out.println("Converting");
        if(!val1Set)
        {
            value1 = Double.parseDouble(displayField.getText());
            val1Set = true;
            System.out.println("Setting Val1: " + value1);
        }
        else
        {
            value2 = Double.parseDouble(displayField.getText());
            System.out.println("Setting Val2: " + value2);
        }
    }

    private void checkOperandState(char op)
    {
        if(op == '.')
        {
            addPoint();
            return;
        }
        if(setOperand == 'N' && op != '=')
        {
            setOperand = op;
            convertToDouble();
            canClearInputField = true;
            return;
        }
        if(setOperand != 'N' && op == '=')
        {
            convertToDouble();
            performOperand();
            canClearInputField = true;
            val1Set = false;
            setOperand = 'N';
            return;
        }
        if(setOperand == 'N' && op == '=')
        {
            return;
        }
        convertToDouble();
        performOperand();
        setOperand = op;
        canClearInputField = true;
    }

    private void addPoint()
    {
        clearInputField();
        displayField.setText("0");
        for(char c : displayField.getText().toCharArray())
        {
            if(c == '.')
            {
                return;
            }
        }
        displayField.setText(displayField.getText() + '.');
    }

    private void performOperand()
    {
        if(setOperand == '+')
        {
            value1 = value1 + value2;
        }
        if(setOperand == '-')
        {
            value1 = value1 - value2;
        }
        if(setOperand == '*')
        {
            value1 = value1 * value2;
        }
        if(setOperand == '/' && value2 != 0)
        {
            value1 = value1 / value2;
        }
        displayField.setText("" + value1);
    }

    private void clearInputField()
    {
        if(canClearInputField)
        {
            displayField.setText("");
        }
        canClearInputField = false;
    }

    private void clearCalcState()
    {
        value1 = 0;
        setOperand = 'N';
        canClearInputField = false;
        val1Set = false;
        operandSelected = false;
        displayField.setText("0");
    }

    private void checkInput(char buttonin)
    {
        if (buttonin == 'C')
        {
            clearCalcState();
            return;
        }
        for(char c: numbers)
        {
            if (c == buttonin)
            {
                addNumber(buttonin);
            }
        }
        for(char c: operands)
        {
            if(c == buttonin)
            {
                checkOperandState(buttonin);
            }
        }
    }

}
